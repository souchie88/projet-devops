# Utiliser une image de base officielle Node.js
FROM node:18

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers package.json et package-lock.json
COPY backend/package*.json ./backend/

# Installer les dépendances pour le backend
RUN cd backend && npm install

# Copier tout le contenu du projet dans le conteneur
COPY . .

# Exposer le port de l'application
EXPOSE 3000

# Démarrer l'application backend et le serveur statique frontend
CMD ["node", "backend/app.js"]