Voici le README.md complet avec les étapes ajoutées : 

---

# Projet DevOps

## Table des matières
- [Pré-requis](#pré-requis)
- [Installer le projet en local](#installation-du-projet-en-local)
- [Lancer l'application en local](#lancer-lapplication-en-local)
- [Pipeline CI/CD](#pipeline-cicd)
- [Lancer l'Application via l'image Docker déployée via la CI/CD](#lancer-lapplication-via-limage-docker-déployée-via-la-cicd)
- [Logs](#logs)

## Pré-requis

Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Un compte sur [GitLab](https://gitlab.com) (si vous souhaitez contribuer ou utiliser le CI/CD)

## Installer le projet en local

1. Clonez le dépôt GitLab :

   ```bash
   git clone https://gitlab.com/souchie88/projet-devops.git
   ```

2. Accédez au répertoire du projet :

   ```bash
   cd projet-devops
   ```

## Lancer l'application en local

1. Construisez les images Docker et démarrez les conteneurs :

   ```bash
   docker-compose up --build
   ```

   Cette commande construira les images Docker nécessaires et démarrera les conteneurs définis dans le fichier `docker-compose.yml`.

2. Accédez au formulaire de l'application dans votre navigateur :

   ```
   http://localhost:3000
   ```

   Accédez à la liste des fichiers CSV de l'application dans votre navigateur :

   ```
   http://localhost:3000/listFiles.html
   ```

## Pipeline CI/CD

Le projet utilise GitLab CI/CD pour automatiser le processus de build, test et déploiement. La configuration CI/CD est définie dans le fichier `.gitlab-ci.yml`.

### Structure de la Pipeline

- **Build Stage** : Construit l'image Docker et la pousse vers le registre Docker de GitLab.
- **Tests Stage** : Exécute les tests définis dans le projet.
- **Deploy Stage** : Déploie l'application en utilisant Docker Compose.


## Lancer l'Application via l'image Docker déployé via la CI/CD


1. Récupérez l'image Docker la plus récente depuis le registre GitLab :

   ```bash
   docker pull registry.gitlab.com/souchie88/projet-devops:latest
   ```

2. Lancez un nouveau conteneur en utilisant l'image Docker récupérée :

   ```bash
   docker run -d -p 3000:3000 registry.gitlab.com/souchie88/projet-devops:latest
   ```

3. Vérifiez le nom du conteneur en cours d'exécution :

   ```bash
   docker ps
   ```

   Exemple de sortie :

   ```
   CONTAINER ID   IMAGE                     COMMAND                  CREATED       STATUS       PORTS                                       NAMES
   aaa2d24fd31e   projet-devops-backfront   "docker-entrypoint.s…"   3 hours ago   Up 3 hours   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   projet-devops-backfront-1
   ```

4. Accédez au conteneur en cours d'exécution via son ID :

   ```bash
   docker exec -it <CONTAINER_ID> /bin/bash
   ```

   Exemple :

   ```bash
   docker exec -it aaa2d24fd31e /bin/bash
   ```

## Logs

Les logs de chaque étape sont stockés dans des fichiers spécifiques dans le répertoire `build_logs` et dans la section artifacts de GitLab [ici](https://gitlab.com/souchie88/projet-devops/-/artifacts).