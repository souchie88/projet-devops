const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files from the frontend directory
app.use(express.static(path.join(__dirname, '../frontend')));

// Endpoint to handle form submissions
app.post('/saveData', (req, res) => {
    const data = req.body;
    const date = new Date().toISOString().split('T')[0]; // Obtient la date actuelle au format YYYY-MM-DD
    const fileName = `jpo_${date}_${data.email}.csv`;
    const filePath = path.join(__dirname, 'csvFiles', fileName);

    // Check if the directory exists, create it if necessary
    const csvDir = path.join(__dirname, 'csvFiles');
    fs.promises.mkdir(csvDir, { recursive: true }) // Use promises for async handling
        .then(() => {
            // Convert data to CSV format
            const csvData = `${data.nom},${data.prenom},${data.telephone},${data.email},${data.formation},${data.options1.join('|')},${data.options2.join('|' )}\n`;

            // Append data to the CSV file
            return fs.promises.appendFile(filePath, csvData);
        })
        .then(() => {
            res.status(200).json({ message: 'Data saved successfully' });
        })
        .catch((err) => {
            console.error('Error saving data:', err);
            res.status(500).json({ message: 'Internal server error' });
        });
});

// Endpoint pour lister tous les fichiers CSV
app.get('/listFiles', (req, res) => {
    const csvDir = path.join(__dirname, 'csvFiles');
    fs.readdir(csvDir, (err, files) => {
        if (err) {
            console.error('Erreur lors de la lecture du répertoire :', err);
            res.status(500).json({ message: 'Erreur interne du serveur' });
        } else {
            res.status(200).json({ files });
        }
    });
});

// Endpoint pour afficher le contenu d'un fichier CSV spécifique
app.get('/viewFile/:fileName', (req, res) => {
    const filePath = path.join(__dirname, 'csvFiles', req.params.fileName);
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Erreur lors de la lecture du fichier :', err);
            res.status(500).json({ message: 'Erreur interne du serveur' });
        } else {
            res.status(200).send(data);
        }
    });
});

// Endpoint to handle file downloads
app.get('/download/:fileName', (req, res) => {
    const fileName = req.params.fileName;
    const filePath = path.join(__dirname, 'csvFiles', fileName);

    fs.readFile(filePath, (err, data) => {
        if (err) {
            console.error('Error reading file for download:', err);
            res.status(500).json({ message: 'Internal server error' });
        } else {
            // Set appropriate HTTP headers for download
            res.setHeader('Content-Type', 'text/csv');
            res.setHeader('Content-Disposition', `attachment; filename="${fileName}`);
            res.send(data);
        }
    });
});


app.listen(port, () => {
    console.log(`Le serveur fonctionne sur http://localhost:${port}`);
});
