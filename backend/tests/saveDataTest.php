<?php

use PHPUnit\Framework\TestCase;

class saveDataTest extends TestCase
{
    public function testInputNotEmpty()
    {
        // Données de test
        $data = [
            "nom" => "Doe",
            "prenom" => "John",
            "telephone" => "1234567890",
            "email" => "john.doe@example.com",
            "formation" => "Informatique",
            "options1" => ["option1", "option2"],
            "options2" => ["option3", "option4"]
        ];

        // Vérification des champs non vides
        $this->assertNotEmpty($data['nom'], 'Nom est vide');
        $this->assertNotEmpty($data['prenom'], 'Prénom est vide');
        $this->assertNotEmpty($data['telephone'], 'Téléphone est vide');
        $this->assertNotEmpty($data['email'], 'Email est vide');
        $this->assertNotEmpty($data['formation'], 'Formation est vide');
    }

    public function testSpecialChars()
    {
        // Données de test
        $data = [
            "nom" => "Doe",
            "prenom" => "John",
            "telephone" => "1234567890",
            "email" => "john.doe@example.com",
            "formation" => "Informatique",
            "options1" => ["option1", "option2"],
            "options2" => ["option3", "option4"]
        ];

        // Vérification de l'absence de caractères spéciaux dans le nom, prénom, et téléphone
        $this->assertMatchesRegularExpression('/^[a-zA-Z]+$/', $data['nom'], 'Nom contient des caractères spéciaux');
        $this->assertMatchesRegularExpression('/^[a-zA-Z]+$/', $data['prenom'], 'Prenom contient des caractères spéciaux');
        $this->assertMatchesRegularExpression('/^[0-9]+$/', $data['telephone'], 'Telephone contient des caractères spéciaux');
    }

    public function testValidEmail()
    {
        // Données de test
        $data = [
            "nom" => "Doe",
            "prenom" => "John",
            "telephone" => "1234567890",
            "email" => "john.doe@example.com",
            "formation" => "Informatique",
            "options1" => ["option1", "option2"],
            "options2" => ["option3", "option4"]
        ];


        // Vérification de la validité de l'email
        $this->assertTrue(filter_var($data['email'], FILTER_VALIDATE_EMAIL) !== false, 'Email is not valid');
    }

}
